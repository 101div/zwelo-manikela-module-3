import 'package:flutter/material.dart';
import 'dashboard.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Your Profile"),
        centerTitle: true,
      ),
      body: const FormFields(),
    );
  }
}

//class to handle the form and input fields
class FormFields extends StatelessWidget {
  const FormFields({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        //crossAxisAlignment: CrossAxisAlignment.start,
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 18),
          child: TextField(
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Enter Your Username",
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 18),
          child: TextField(
              decoration: InputDecoration(
            border: UnderlineInputBorder(),
            labelText: "First Name",
          )),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 18),
          child: TextField(
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Last Name",
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 18),
          child: TextField(
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "Email Address",
            ),
          ),
        ),
        const Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 18),
          child: TextField(
            obscureText: true,
            decoration: InputDecoration(
              border: UnderlineInputBorder(),
              labelText: "New Password",
            ),
          ),
        ),
        const SizedBox(
          height: 50,
        ),
        ElevatedButton(
          onPressed: () => {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const Dashboard()))
          },
          child: const Text("Update"),
          style: ElevatedButton.styleFrom(
            fixedSize: const Size(300, 50),
          ),
        ),
      ],
    );
  }
}
