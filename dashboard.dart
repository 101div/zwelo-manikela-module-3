import 'package:flutter/material.dart';
import 'profile.dart';
import 'bmiCalculator.dart';
import 'temperature.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Home"),
          centerTitle: true,
        ),
        body: Center(
            child: Column(
          /* two button on the home screen
              *  one for the BMI calculation screen
              *  one for temperature conversion
              */
          children: <Widget>[
            const SizedBox(height: 50),
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const BMICalculator())),
              },
              child: const Text("Calculate Body Mass Index (BMI)"),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(400, 80),
              ),
            ),
            //button for the temperature conversion screen
            const SizedBox(height: 50),
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const TEMPERATURE()))
              },
              child: const Text("Temperature Converter"),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(400, 80),
              ),
            )
          ],
        )),
        //button to the edit profile screen
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () => {
            Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => const Profile()))
          },
          label: const Text("Edit Profile"),
          icon: const Icon(Icons.edit),
        ));
  }
}
