import 'package:flutter/material.dart';

class BMICalculator extends StatelessWidget {
  const BMICalculator({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("BMI Calculator"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(children: <Widget>[
          const Padding(
            padding: EdgeInsets.all(15),
            child: TextField(
              decoration: InputDecoration(
                border: UnderlineInputBorder(),
                labelText: "Weight(kg)",
                hintText: "Enter your weight",
              ),
            ),
          ),
          const Padding(
            padding: EdgeInsets.all(15),
            child: TextField(
              decoration: InputDecoration(
                border: UnderlineInputBorder(),
                labelText: "Height(meters)",
                hintText: "Enter your height",
              ),
            ),
          ),
          const SizedBox(height: 50),
          ElevatedButton(
            onPressed: () => {},
            child: const Text("Calculate"),
            style: ElevatedButton.styleFrom(
              fixedSize: const Size(300, 50),
            ),
          ),
        ]),
      ),
    );
  }
}
